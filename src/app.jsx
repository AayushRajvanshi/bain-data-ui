import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './configureStore';

const store = configureStore();

//Styles
require('./styles/app.scss');

//Components
import TestContainer from './containers/TestContainer.jsx';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route exact path="/" component={TestContainer} />
    </Router>
  </Provider>,
  document.getElementById('root')
);
