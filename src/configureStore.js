import { createStore, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import reducer from './reducers/index';

const configureStore = (preloadedState) => {
    const enhancer = compose(
        applyMiddleware(thunk)
    );
    const store = createStore(
        reducer,
        preloadedState,
        enhancer
    );
    return store;
}

export default configureStore;