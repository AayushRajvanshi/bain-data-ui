import axios from 'axios';

export const setInputs = (e) => {
    let input = {};
    input[e.target.name] = e.target.value;
    return {
        type: 'SET_INPUT',
        data: { ...input }
    };
};

export const submitForm = (event) => {
    event.preventDefault();
    return (dispatch, getState) => {
        dispatch(startProviderFetch());
        const currentPage = getState().providers.currentPage;
        const state = getState().providers.data.state;
        const min_discharges = getState().providers.data.min_discharges;
        const max_discharges = getState().providers.data.max_discharges;
        const min_average_covered_charges = getState().providers.data.min_average_covered_charges;
        const max_average_covered_charges = getState().providers.data.max_average_covered_charges;
        const min_average_medicare_payments = getState().providers.data.min_average_medicare_payments;
        const max_average_medicare_payments = getState().providers.data.max_average_medicare_payments;
        const url = `https://bain-data-api.herokuapp.com/providers-without-auth?limit=12&page=${currentPage}&state=${state}&min_discharges=${min_discharges}&max_discharges=${max_discharges}&min_average_covered_charges=${min_average_covered_charges}&max_average_covered_charges=${max_average_covered_charges}&min_average_medicare_payments=${min_average_medicare_payments}&max_average_medicare_payments=${max_average_medicare_payments}`;
        axios.get(url).then((res) => {
            dispatch(completeProviderFetchSuccess(res.data));
        }).catch((err) => {
            dispatch(completeProviderFetchFailure());
            console.log(err);
        });
    };
};

const startProviderFetch = () => {
    return {
        type: 'START_PROVIDER_FETCH'
    };
};

const completeProviderFetchSuccess = (data) => {
    return {
        type: 'COMPLETE_PROVIDER_FETCH_SUCCESS',
        data
    };
};
const completeProviderFetchFailure = (data) => {
    return {
        type: 'COMPLETE_PROVIDER_FETCH_FAILURE',
        data
    };
};

const startChangePage = (data) => {
    return {
        type: 'START_CHANGE_PAGE',
        data
    };
};

export const clearForm = (e) => {
    e.preventDefault();
    return {
        type: 'CLEAR_SEARCH'
    };
};

export const changePage = (data) => {
    return (dispatch, getState) => {
        dispatch(startChangePage(data));
        const currentPage = getState().providers.currentPage;
        const state = getState().providers.data.state;
        const min_discharges = getState().providers.data.min_discharges;
        const max_discharges = getState().providers.data.max_discharges;
        const min_average_covered_charges = getState().providers.data.min_average_covered_charges;
        const max_average_covered_charges = getState().providers.data.max_average_covered_charges;
        const min_average_medicare_payments = getState().providers.data.min_average_medicare_payments;
        const max_average_medicare_payments = getState().providers.data.max_average_medicare_payments;
        const url = `https://bain-data-api.herokuapp.com/providers-without-auth?limit=12&page=${currentPage}&state=${state}&min_discharges=${min_discharges}&max_discharges=${max_discharges}&min_average_covered_charges=${min_average_covered_charges}&max_average_covered_charges=${max_average_covered_charges}&min_average_medicare_payments=${min_average_medicare_payments}&max_average_medicare_payments=${max_average_medicare_payments}`;
        axios.get(url).then((res) => {
            dispatch(completeProviderFetchSuccess(res.data));
        }).catch((err) => {
            dispatch(completeProviderFetchFailure());
            console.log(err);
        });
    };
};