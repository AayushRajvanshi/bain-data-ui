import { combineReducers } from 'redux';

const initialState = {
    data: {
        state: '',
        min_discharges: '',
        max_discharges: '',
        min_average_covered_charges: '',
        max_average_covered_charges: '',
        min_average_medicare_payments: '',
        max_average_medicare_payments: ''
    },
    providers: [],
    isFetching: false,
    currentPage: 1
}

export var providersReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_INPUT':
            return {
                ...state,
                data: {
                    ...state.data,
                    ...action.data
                }
            };
        case 'START_PROVIDER_FETCH':
            return {
                ...state,
                isFetching: true,
                providers: undefined
            };
        case 'COMPLETE_PROVIDER_FETCH_SUCCESS':
            return {
                ...state,
                isFetching: false,
                providers: action.data
            };
        case 'COMPLETE_PROVIDER_FETCH_FAILURE':
            return {
                ...state,
                isFetching: false,
                providers: action.data
            };
        case 'START_CHANGE_PAGE':
            return {
                ...state,
                isFetching: true,
                currentPage: state.currentPage + action.data
            };
        case 'CLEAR_SEARCH':
            return initialState;
        default:
            return state;
    }
};


const reducer = combineReducers({
    providers: providersReducer
});


export default reducer;