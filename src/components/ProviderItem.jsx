import React from 'react';

const ProviderItem = (props) => {
    return (
        <div>
            <div className="uk-card uk-card-default uk-padding-small">
                <div className="uk-card-body">
                    <p className="uk-text-bold"> {props["Provider Name"]}</p>
                    <p>{props["Provider Street Address"]} {props["Provider State"]} {props["Provider Zip Code"]}</p>
                    <p className="uk-margin-remove"><span className="uk-text-bold">Total Discharges - </span>{props["Total Discharges"]}</p>
                    <p className="uk-margin-remove"><span className="uk-text-bold">Average Covered Charges - </span>{props["Average Covered Charges"]}</p>
                    <p className="uk-margin-remove"><span className="uk-text-bold">Average Medicare Payments - </span>{props["Average Medicare Payments"]}</p>
                </div>
            </div>
        </div>
    );
}

export default ProviderItem;