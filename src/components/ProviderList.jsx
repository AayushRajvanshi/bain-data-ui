import React from 'react';

const ProviderList = ({ children }) => {
    return (
        <div className="uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-small uk-grid-match" data-uk-grid>
            {children}
        </div>
    );
};

export default ProviderList;