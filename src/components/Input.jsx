import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setInputs, submitForm, clearForm } from './../actions';

class Input extends Component {
    render() {
        const { data, dispatchUpdateInputs, dispatchFormSubmit, dispatchClearForm } = this.props;
        return (
            <form className="uk-grid-small" data-uk-grid onSubmit={(e) => dispatchFormSubmit(e)}>
                <div className="uk-width-1-1@s">
                    <input id="state" name="state" className="uk-input" value={data.state} type="text" placeholder="State" onChange={(e) => dispatchUpdateInputs(e)} />
                </div>
                <div className="uk-width-1-2@s">
                    <input id="min_discharges" name="min_discharges" value={data.min_discharges} className="uk-input" type="text" placeholder="Min Discharges" onChange={(e) => dispatchUpdateInputs(e)} />
                </div>
                <div className="uk-width-1-2@s">
                    <input id="max_discharges" name="max_discharges" value={data.max_discharges} className="uk-input" type="text" placeholder="Max Discharges" onChange={(e) => dispatchUpdateInputs(e)} />
                </div>
                <div className="uk-width-1-2@s">
                    <input id="min_average_covered_charges" name="min_average_covered_charges" value={data.min_average_covered_charges} className="uk-input" type="text" placeholder="Min Average Covered Charges" onChange={(e) => dispatchUpdateInputs(e)} />
                </div>
                <div className="uk-width-1-2@s">
                    <input id="max_average_covered_charges" name="max_average_covered_charges" value={data.max_average_covered_charges} className="uk-input" type="text" placeholder="Max Average Covered Charges" onChange={(e) => dispatchUpdateInputs(e)} />
                </div>
                <div className="uk-width-1-2@s">
                    <input id="min_average_medicare_payments" name="min_average_medicare_payments" value={data.min_average_medicare_payments} className="uk-input" type="text" placeholder="Min Average Medicare Payments" onChange={(e) => dispatchUpdateInputs(e)} />
                </div>
                <div className="uk-width-1-2@s">
                    <input id="max_average_medicare_payments" name="max_average_medicare_payments" value={data.max_average_medicare_payments} className="uk-input" type="text" placeholder="Max Average Medicare Payments" onChange={(e) => dispatchUpdateInputs(e)} />
                </div>
                <div className="uk-width-1-2@s">
                    <button className="uk-button uk-button-primary uk-width-1-1" type="submit">Search</button>
                </div>
                <div className="uk-width-1-2@s">
                    <button className="uk-button uk-button-primary uk-width-1-1" type="button" onClick={dispatchClearForm}>CLEAR</button>
                </div>
            </form>
        );
    }
}

const mapStateToProps = state => {
    const { providers } = state;
    return providers;
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatchUpdateInputs: (event) => {
            dispatch(setInputs(event))
        },
        dispatchFormSubmit: (event) => {
            dispatch(submitForm(event))
        },
        dispatchClearForm: (event) => {
            dispatch(clearForm(event))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Input);