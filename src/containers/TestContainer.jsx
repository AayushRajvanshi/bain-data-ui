import React, { Component } from 'react';
import { connect } from 'react-redux';
import Input from './../components/Input.jsx';
import ProviderItem from './../components/ProviderItem.jsx';
import ProviderList from './../components/ProviderList.jsx';
import { setResultsFrom, fetchCompany, changePage } from './../actions/index';

class TestContainer extends Component {
    handlePrevious = () => {
        this.props.changePage(-1);
    }
    handleNext = () => {
        this.props.changePage(1);
    }
    render() {
        const { isFetching, providers, currentPage } = this.props;
        const providersList = providers ? providers.total > 0 ? providers.docs.map((provider, index) => {
            return <ProviderItem key={index} {...provider} />
        }) : <div className="uk-text-center">No result Found</div> : null;
        const node = isFetching ? <div className="uk-section uk-section-default uk-height-large uk-text-center">Loading...</div> :
            <ProviderList >
                {providersList}
            </ProviderList >
        return (
            <div>
                <div className="uk-section uk-section-default uk-preserve-color">
                    <div className="uk-container uk-container-small">
                        <div className="uk-h3 uk-text-uppercase uk-text-center">Bain & Company</div>
                        <Input />
                        {providers && providers.total > 0 ? <hr /> : null}
                        {providers && providers.total > 0 ? <div className="uk-margin uk-flex uk-flex-between">
                            <button className="uk-button uk-button-secondary" disabled={currentPage === 1} onClick={this.handlePrevious}>Previous</button>
                            <p className="uk-h6 uk-text-bold">Total {providers.total}</p>
                            <button className="uk-button uk-button-secondary" disabled={currentPage === providers.pages} onClick={this.handleNext}>Next</button>
                        </div> : null}
                        {node}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { isFetching, providers, currentPage } = state.providers;
    return {
        isFetching,
        providers,
        currentPage
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        changePage: (by) => {
            dispatch(changePage(by))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TestContainer);