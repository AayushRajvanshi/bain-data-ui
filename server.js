require("babel-polyfill");

const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const cors = require('cors')
const Client = require('node-rest-client').Client;
const client = new Client();

const axios = require('axios');
app.use(cors())

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'dist')));

app.use(logger('dev'));

app.use(express.static('dist'));

var port = process.env.PORT || 3001;
app.listen(port, () => console.log(`Server initialized on // ${new Date()}`));